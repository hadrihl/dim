#include <stdio.h>
#include <stdlib.h>

#define BYTE 1LU
#define KB (BYTE * 1024LU)
#define MB (KB * 1024LU)
#define GB (MB * 1024LU)

__global__ void kernel(int* d_databank) {
	int idx = threadIdx.x + 
		threadIdx.y * blockDim.x + 
		threadIdx.z * blockDim.y * blockDim.x + 
		blockIdx.x * blockDim.z * blockDim.y * blockDim.x + 
		blockIdx.y * gridDim.x * blockDim.z * blockDim.y * blockDim.x;
	d_databank[idx] = 999;
}

int main(int argc, char* argv[]) {
	// prerequisites
	cudaError_t error;

	// var init
	int *databank, *d_databank;
	dim3 grid(2, 2, 1); dim3 block(2, 2, 2);
	int size = grid.x * grid.y * grid.z * block.x * block.y * block.z;
	size_t cudaSize = size * sizeof(int);

	printf("cudaSize = %lu bytes\n", cudaSize);

	// mem alloc
	databank = (int*) calloc(size, sizeof(int));

	error = cudaMalloc((void**) &d_databank, cudaSize);

	if(error != cudaSuccess) { fprintf(stderr, "cudaMalloc failed: d_databank : %s\n", cudaGetErrorString(error)); exit(1); }

	// cuda copy H2D
	error = cudaMemcpy(d_databank, databank, cudaSize, cudaMemcpyHostToDevice);

	if(error != cudaSuccess) { fprintf(stderr, "cudaMemcpy H2D failed: d_databank : %s\n", cudaGetErrorString(error)); exit(1); }
	
	// kernel launches
	kernel<<< grid, block >>>(d_databank);

	// cuda copy D2H
	error = cudaMemcpy(databank, d_databank, cudaSize, cudaMemcpyDeviceToHost);

	if(error != cudaSuccess) { fprintf(stderr, "cudaMemcpy D2H failed: databank : %s\n", cudaGetErrorString(error)); exit(1); }

	// verify
	int i; for(i = 0; i < size; i++) printf("databank[%d] = %d\n", i, databank[i]);

	// clean up mem
	cudaFree(d_databank);
	free(databank);

	return 0;
}
