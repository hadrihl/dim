CUDA = nvcc
CUDA_FLAG = -m64 -arch=sm_12 -g -G

BIN = a.out

all: $(BIN)

a.out: dim.cu
	$(CUDA) $(CUDA_FLAG) -o $@ $<

clean:
	$(RM) $(BIN)
